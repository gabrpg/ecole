package main

import (
	"flag"
	"fmt"
)

func main() {

	deverminagePtr := flag.Bool("d", false, "bool de déverminage")
	nFlag := flag.Int("n", 1234, "veuillez assigner n")

	flag.Parse()

	if *deverminagePtr {
		print("On dévermine\n")
	} else {
		print("On dévermine pas\n")
	}

	fmt.Printf("n: %d\n", *nFlag)
}
