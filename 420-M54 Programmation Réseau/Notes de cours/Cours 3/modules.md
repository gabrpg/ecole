# Hiérarchie d'un projet

## Modules

Un module est une collection de paquetages qui sont donnés, versionnés et distribués ensemble. Ils peuvent être téléchargés directement de serveur de version (GitHub) ou des serveurs mandataires.

Un module est identifié par son chemin et déclaré dans le fichier go.mod. Le répertoire racine est le répertoire contenant le fichier go.mod.

Un paquetage est une collection de fichier .go contenu dans un sous-répertoire du module. Donc par exemple, si nous créons la hiérarchie suivante:

```
/home/drynish/go
                /newmodule
                    go.mod
                    /newpackage1/
                    /newpackage2/
                    
```
Par exemple, le module "golang.org/x/net" contient un paquetage dans le répertoire "html". Pour l'inclure, il faut utiliser "golang.org/x/net/html".

[Paquetage HTML](../../Exemple/Cours%203/exemple/)


## Paquetage à l'intérieur d'un module (local)

Cette ligne permet d'utiliser  `import "local/crlj"` dans notre projet. Cet import pointe vers le dossier local : `local/crlj` situé sous le répertoire principal du module local. 

On peut utiliser un alias avec un import:

```go
import c "local/crlj"
```

# Module en ligne

On peut télécharger un paquetage avec la commande suivante:

```go
go get rsc.io/quote
```

Il sera télécharger dans votre répertoire personnel go. On pourrait aussi juste mettre l'import et utiliser le paquetage, go va télécharger la dépendance à l'exécution au besoin. Par contre, l'autocomplétion ne fonctionnera pas tant qu'il ne sera pas téléchargé.

Pour retirer une dépendance, exécuter la commande suivante qui va enlever les dépendances non utilisées:

```
go mod tidy
```

[callChocolate](../../Exemple/Cours%203/callChocolate/)
[libChocolate](http://www.github.com/drynish/chocolate)

[callDice](../../Exemple/Cours%203/callDice/)
[libDice](http://www.github.com/drynish/patati)

## Créer un module en ligne

* Créez un module local, lorsque vous l'initialisez comme module, veuillez utiliser un nom complet:

```
git init mod github.com/drynish/chocolate
```

* Le pousser sur un gestionnaire de version (github, gitlab, ...)
* Créer un tag > 1 sur ce dernier, car sinon il est considéré comme bêta. Par exemple:

```git
git tag v1.0.0
```

* Visitez l'adresse suivante: https://pkg.go.dev/example.com/my/module

  * example.com peut être remplacer par github
  * my par votre username 
  * module par le nom du module choisi.

https://pkg.go.dev/about#adding-a-package

* Attendre patiemment avec un jus. (peut prendre quelques heures)

## Utilisation d'un package local au lieu de github

Pour indiquer à Go d'utiliser un package local au lieu d'en ligne (et d'éviter de le télécharger s'il n'exite pas):

Dans le fichier go.mod, vous indiquez que le package recherché est local.

```go
require github.com/drynish/lib v0.0.0
replace github.com/drynish/lib => ./github.com/drynish/lib
```

Lorsque votre fichier a été téléverser sur github, vous pouvez enlever ces références locales.
