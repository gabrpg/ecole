# Les bases d'une Application Android

https://developer.android.com/guide/components/fundamentals

Les applications Android peuvent être écrites en Kotlin, Java ou en C++. En utilisant un SDK vous compilez votre code et toutes ressources nécessaires en APK ou un Android App Bundle (aab).

* Un Package Android est un fichier d'archive avec le suffixe .apk. Il contient tout ce qui est nécessaire pour installer une application.

* Un Android App Bundle est un fichier archive contenant un projet Android et des données additionnelles. Ceci reste un format de publication qui ne peut être installer sur un Android.

Lorsque vous distribuez votre application sur Google Play, les serveurs de Google Plays génèrent que des APKs optimisés contenant que les ressources et codes nécessaires demandés par ledit téléphone demandant l'installation de l'application.

L'univers Android est basé sur le principe que chaque application vit dans son propre espace sandbox suivant les règles de sécurité suivantes:

* Android est un système d'exploitation multi-utilisateur dans lequel chaque application est un utilisateur différent.

* Par défaut, le système affecte à chaque application un utilisateur Linux différent (avec un ID). Le système utilise cet ID pour tous les fichiers accessibles par l'application et les autres applications (roulant avec un ID différent) ne peuvent accéder au fichier de l'application.

* Chaque processus a sa propre machine virtuelle, donc chaque code d'application roule en isolation avec les autres applications.

* Chaque application s’exécute dans son propre processus Linux. Le système d'exploitation Android commence le processus Linux quand n'importe quelle portion de l'application doit être exécutée et termine le processus quand il n'est plus nécessaire.


Le système d'exploitation se base sur le principe du moindre privilège. Chaque application a accès aux composants dont il a besoin et n’obtient rien de plus.

Il est possible cependant de partager entre plusieurs applications:

* Une application pourrait partager les mêmes certificats et donc, par surcroît avoir accès aux mêmes ressources.

* Une application peut demander des permissions supplémentaires et donc d'avoir accès au positionnement, à la caméra, au Bluetooth ... Nous verrons ultérieurement dans le cours les notions de Permissions.

# 4 types de composants

* Activités
* Services
* Broadcast receivers
* Content providers

## Activitées

L'activité est le point d'entrée pour interagir avec l'utilisateur. Il représente un simple écran d'une interface utilisateur. Par exemple, pour une application de courriel:

* Une activité représenterait la liste des courriels
* Une autre pour composer un courriel
* Une autre pour lire un courriel

Même si les activités servent à coordonner l'expérience utilisateur dans l'application, chaque activité est indépendante. Une activité externe pourrait démarrer une de ces 3 activités si l’appel est permis à l'extérieur. Par exemple, une application photo pourrait permettre de composer un courriel afin de permettre à l'utilisateur de partager une photo.

## Services

Le service est un point d'entrée afin de garder une application en arrière-plan. Un service n'offre pas d'interface utilisateur. Le meilleur exemple est un lecteur de musique, il permet de jouer de la musique en arrière-plan même si l'utilisateur est dans une application différente.

## Broadcast Receiver

Un Broadcast receiver est un composant qui permet de répondre à des annonces à tous. Puisque chaque broadcast receiver sont définis à l'intérieur de l'application, le système peut déliver un broadcast à toutes les applications même celles qui ne sont pas en cours d'exécution.

Par exemple, une application peut programmer une alarme lorsque la batterie est basse (BATTERY_LOW) ou qu'on vient de changer de mobile à WIFI.

Plusieurs broadcasts proviennent du système d'exploitation. Généralement le broadcast est une passerelle afin d'aviser qu'une situation est survenue, par la suite un JobService pourrait être implémenter pour prendre le prendre en charge.

## Content providers

Un content provider permet de partager des données d'une application provenant du système de fichier, d’une base de données SQLite, du web ou de tout autre stockage persistent de stockage de l'application. Par l'entremise du content provider, les autres applications peuvent questionner ou modifier les données.

Par exemple, le système Android offre un content provider pour les informations de contacts de l'utilisateur.

Les Content provides sont aussi utiles lorsque vient le temps de lire ou d'écrire des données privées de votre application et que vous ne voulez pas que données soient partagées.

# Intentions

Un aspect intéressant d'Android: toutes les applications peuvent exécuter un composant d'une autre application. Par exemple, si votre application souhaite capturer une photo de la caméra, il y a de fortes possibilités que vous puissiez utiliser une application tierce pour se faire. Vous ne devez pas coder ou lier votre code à l'application tierce, à la place, vous lancez une nouvelle activité afin de capturer une photo de l'application Caméra. Quand la photo est prise, cette dernière est tout simplement retournée à votre application vous permettant donc de l'utiliser. Pour l'utilisateur, c'est directement votre application qui a pris la photo.

Lorsque vous lancez un composant d'application comme ça, le processus lancé n'appartient pas à votre application, mais bien à celle de l'application caméra. Pour permettre ceci, on utilise un message système nommé Intention (ou Intent).

Nous verrons les intentions davantage au prochain cours.


# Le fichier Manifest

Avant que le système Android puisse commencer un composant d'application, le système doit savoir que le composant existe en lisant le fichier AndroidManifest.xml de l'application. L'application déclare tous ses composants dans ce fichier, situé à la racine du répertoire de l'application Android.

Voici ce que fait le fichier en plus de déclarer les composants:

* Identifier les permissions utilisateurs que l'application requiert (par exemple accès à Internet)
* La version minimal d'API de l'application.
* Déclaration des fonctionnalités matériel et logiciel utilisés ou requis par l'application.
* ...

## Déclaration de composants

La tâche principale du manifest est d'informer le système des composants. La déclaration des activités est faite ainsi:

```xml

<?xml version="1.0" encoding="utf-8"?>
<manifest ... >
    <application android:icon="@drawable/app_icon.png" ... >
        <activity android:name="com.example.project.ExampleActivity"
                  android:label="@string/example_label" ... >
        </activity>
        ...
    </application>
</manifest>
```

Dans l'élément <application>, l'android:icon pointe à la ressource système contenant l'icône de l'application.

Dans l'élément <activity>, l'attribut android:name spécifie le nom complet de la sous-classe (dérivée) de l'activité, et le android:label permet de spécifier la chaîne utilisée pour le nom de l'application.

Vous devez déclarer tous les composants d'application en utilisant les éléments suivants:

* <activity> elements 
* <service> elements
* <receiver> elements
* <provider> elements

Toutes activitées, services, ou content providers inclus dans votre code source mais non inclus dans le manifeste ne sera jamais exécutable. 

## Déclaration des exigences

```
build.gradle:


android {
  ...
  defaultConfig {
    ...
    minSdkVersion 26
    targetSdkVersion 29
  }
}
```

Note:

N'affectez pas ces deux variables dans le manifest car ils seront réécrits par l'entremise de Gradle lors de la compilation.

**uses-features** permet de spécifier à Google Play que qqch est nécessaire si on veut utiliser cette application.

<manifest ... >
    <uses-feature android:name="android.hardware.camera.any"
                  android:required="true" />
    ...
</manifest>


### Comment Android fait-il pour savoir quelle activité lancée lorsqu'il roule notre application ? 

À l'intérieur du manifest, il y a une section nommée intent-filter dans la description de notre  activité qui indique l'activité à lancer lors de l'ouverture de cette application.

## Projet Android

Lors de la création d'une application android, une multitude de fichiers et répertoires sont créés :

* **java** : Répertoire du code source de l'application (en java ou kotlin) qui sera compilé ultérieurement. C'est la que vous allez créer le code qui permettra à votre code d'exécuter des opérations suite à un clic de bouton ou à un choix dans une liste.

* **java (generated)** : Répertoire contenant votre code compilé de votre application.

* **res** : Répertoire contenant les ressources de votre application. Il y a entre autres les XML pour la création des interfaces, les fichiers images, les fichiers raws (sons, ...)

* **manifests** : contient la définition de votre application et ses paramètres

* **build.gradle**: fichier de configuration de gradle

## Les ressources Android

Les ressources sont des éléments d'une programmation qui ne doivent pas être présents dans le code source directement par exemple des chaînes de caractères ou des images. Une ressource est un fichier ou une valeur qui est dans le répertoire /res d'une application.

Voici des exemples de ressources :

* **Strings** : Les chaînes de caractères
* **Drawable** : Les images (Bitmaps, jpg, png, …)
* **Colors** : Les couleurs des éléments (associé à un nom)
* **Layouts** : Fichiers de layout (présentation des interfaces)
* **Menus** : Menus d'une application Android
* **Raw** : Fichier binaire, par exemple des fichiers audios.
* **XML** : Fichier xml arbitraire (non utilisé par Android)
* **mipmap** : Les fichiers pour le launcher de l'application, finalement c'est l'apparence de votre application dans le launcher.

Il est possible de créer des ressources différentes (options régionales, format en fonction de l'orientation, taille de l'écran …) simplement en créant des fichiers du même nom, mais avec des options différentes dans les différents répertoires.  Le système lors de l'exécution tente de dénicher la configuration optimale.

Il est fortement encouragé d'utiliser le fichier strings.xml plutôt que de hardcoder les chaînes, vous aurez compris pourquoi, lors de la traduction de votre fichier de langue, vous n'avez qu'à envoyer un fichier avec les valeurs.

## String.xml

Il est aisé de rendre votre application multilingue, il suffit de créer un répertoire additionnel dans le répertoire /res nommé /values-fr/ qui prendra préséance sur le répertoire /values, si l'environnement est en français.

À l'intérieur d'Android Studio, vous verrez une interface simple afin d'ajouter vos propres strings. Voici comment accéder aux données à l'intérieur de la ressource string exemple :

```
val exemple = getString(R.string.exemple)
```

## Layout.xml

Il est possible d'utiliser plusieurs fichiers layouts dans une application afin de lui permettre d'être plus appropriée lors de changement de la présentation :

* Appareil différent (montre, télé, téléphone, tablette)
* Portrait vs Paysage
* Jour ou nuit
* Version du framework
...

## Views

Il y a deux types d'éléments Views :

* Layouts: Les conteneurs de views, ils permettent de spécifier le positionnement des éléments à l'intérieur de l'activité. On peut imbriquer les layouts dans un layouts. Ils ont le mot Layout comme suffixe tel que LinearLayout ou ConstraintLayout .

* Views: Les éléments d'interface avec lesquels l'utilisateur interragit : TextView, EditText, ImageView, Button, Chromometer, …

Tous les éléments Views (layouts et objets) doivent spécifier minimalement les propriétés suivantes:

* android_layout_width : largeur de l'objet
* android_layout_height : la hauteur de l'objet
* android_id: nom unique d'identification (pour le code kotlin)

## Layout

Un layout est la façon que les views (objets d'interface) sont positionnées dans une interface. Les différents layouts sont situés dans le répertoire ressources/layouts. Les 3 layouts principaux de bases sont les suivants :

### LinearLayout 

un layout qui affiche ses vues de façon en ordre un à la suite de l'autre soit verticalement ou horizontalement. Auparavant très utilisé mais il commence à disparaître de plus en plus, Android suggère d'utiliser dorénavant le ConstraintLayout à toutes les sauces. Cependant, il est encore supporté et le premier atelier sera sur ce dernier par sa simplicité. Mais de nos jours, les nouvelles applications sont faites avec le ConstraintLayout que nous verrons au prochain cours.

### ConstraintLayout 

Un layout qui affiche ses vues (composantes) de positionnement relatif (par rapport au parent ou à un autre vue) en utilisant des contraintes pour spécifier l'apparence. C'est le layout traditionnel en AndroidStudio et remplaçant du  RelativeLayout.

### GridLayout 

Un layout qui divise l'écran en lignes, colonnes et des cellules. Vous devez spécifié le nombre de colonnes que vous voulez, ou vous voulez que vos composantes apparaissent et le nombre de rangées ou colonnes elles doivent contenir.

## Chargement du layout

La ligne setContentView(R.layout.activity_selecteur_de_biere) à l'intérieur du fichier MainActivity.kt indique le nom du fichier layout à utiliser lors de la création de l'activité 

La classe R est une classe générée dynamiquement qui référencie tout le contenu du répertoire de ressources res après compilation.

La commande setContentView génère le layout en lisant le fichier XML et en générant tous les objets Java contenu dans votre fichier layout.

Comment accéder à nos views à partir du code java ou kotlin ?

Deux étapes sont nécessaires :

1. Créer un identifiant à l'intérieur du layout permettant de le référencer :

```
android:id=”@+id/test”
```

2. À l'intérieur de kotlin :

```
val selectBeer_button : Button = findViewById(R.id.selectBeer_button)
```

### Important

* Le @ indique à Android de ne pas traiter l'intérieur du contenu comme du texte mais bien d'utiliser une référence vers un objet ressource.
* Le + indique à Android d'ajouter ce dernier à la liste des id s'il n'existe pas déjà.
* Le id signifie la catégorie à ajouter, on pourrait ainsi accéder à la liste des styles, strings ou image.
* Finalement, Java ne permet pas d'utiliser le / à l'intérieur des méthodes, alors nous devons le remplacer par un .

Au prochain cours, nous verrons comment effectuer le databinding de kotlin qui permet d'utiliser le nom des variables directement dans notre code (un peu à la visual studio) sans passer par **getElementById** et le nom de l'objet.

## Gestion des événements
Lorsque vous avez une référence pour un objet, vous pouvez leur ajouter des événement ou modifier leur propriété:

```
var tv = findViewById<TextView>(R.id.textView)
tv.text = "bonjour"
```

L'ajout d'événement peut se faire de deux façons:

XML: en utilisant le paramètre onclick= dans la déclaration XML d'un objet (ou le designer). Vous devez spécifiez le nom d'une méthode du nom que vous souhaitez appelé:

```
<Button android:id="@+id/button"
android:layout_width="match_parent"
android:layout_height="wrap_content"
android:onclick="patate" // ici
android:text="Appuyez ici">
```

À l'intérieur du fichier MainActivity.kt, on ajoute la méthode suivante:

```
// Remarquez le nom de la méthode et l'objet reçu
fun patate(view: View) { 
   tv.text = "bonjour"
}
```

Faisons une petite historique de l'amélioration de la façon d'assigner une méthode onclick:

Au début (en java), il fallait déclarer un objet OnClickListener afin de redéfinir sa méthode onClick pour indiquer ce que nous souhaitons effectuer.  

```
view.setOnClickListener(object : View.OnClickListener {
   override fun onClick(v: View?) {
      toast("Hello") 
   }
})
```

En utilisant les fonctions lambdas, on est capable d'y affecter directement une méthode lambda à v. L'éditeur vous proposera d'utiliser ce genre d'assignation

```
view.setOnClickListener({ v -> toast("Hello") })
```

Si la dernière portion est une méthode, elle peut être sortie des paranthèses, si vous aviez à passer des arguments à la méthode setOnClickListener, vous devriez les mettre à l'intérieur et sortir l'événement.

```
view.setOnClickListener() { v -> toast("Hello") }
```

Si setOnClickListener n'a qu'un seul paramètre, on peut supprimer les paranthèses.

```
view.setOnClickListener { v -> toast("Hello") }
```

Si vous n'avez pas besoin de v, l'objet appelant.
```
view.setOnClickListener { toast("Hello") }
```

Si vous en avez besoin, vous pouvez le passez en paramètre à une méthode

```
view.setOnClickListener { v -> doSomething(v) }
```

Ou tout simplement utiliser l'objet it créé automatiquement.

```
view.setOnClickListener { doSomething(it) }
```

Exemple

```
button.setOnClickListener {
   tv.text = "bonjour"
   if (it !is Button) return@setOnClickListener
   it.text = "Carotte"
}
```


