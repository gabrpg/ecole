package main

import "fmt"

func Moyenne(data []float64) float64 {
	var sum = 0.0
	for _, j := range data {
		sum += j
	}

	return sum / float64(len(data))
}

func main() {
	fmt.Printf("%f\n", Moyenne([]float64{1, 2, 3, 8, 7.3}))
}
