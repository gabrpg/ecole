open class Animal {

  open var couleur = ""
  open var food = ""
  open var habitat = ""

  open fun crie() {
  }
  
  open fun mange() {
  }
  
  open var nbrPattes = 0
  get() = field
  
  set(value) {
      field = value 
  }
}

class Hippo : Animal() {
  override var couleur = "gris"
  override var food = "herbe"
  override var habitat = "eau des lacs africains"

  override fun crie() {
    println("Il émet des mugissements")
  }

  override fun mange() {
    println("L'hippopotame mange de l'$food")
  }

}

class Chien : Animal() {
  override var couleur = "brun"
  override var food = "bouffe à chien"
  override var habitat = "maison"

  override fun crie() {
    println("Il émet des aboiements")
  }

  override fun mange() {
    println("Le chien mange de la $food")
  }

}

fun main(){
	var list = listOf(Hippo(), Chien())

  for (a in list) {
    a.mange()
  }
}