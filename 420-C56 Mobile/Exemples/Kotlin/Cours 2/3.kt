open class Animal {

  open var couleur = ""
  open var food = ""
  open var habitat = ""

  open fun crie() {

  }
  
  open fun mange() {

  }
  
  open var nbrPattes = 0
  get() = field
  
  set(value) {
      field = value 
  }
}

class Hippo : Animal() {
  override var couleur = "gris"
  override var food = "herbe"
  override var habitat = "eau des lacs africains"

  override fun crie() {
    println("Il émet des mugissements")
  }

  override fun mange() {
    println("L'hippopotame mange de l'$food")
  }

}

fun main(){
	var a = Hippo()
  a.habitat = "Patate"
  a.nbrPattes = 5
  a.mange()
}