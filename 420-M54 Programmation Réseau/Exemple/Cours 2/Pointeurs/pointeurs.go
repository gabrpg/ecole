package main

import (
	"fmt"
)

func one(abc int) {
	abc = 1
	print("From one: ")
	println(abc)
	println(&abc)
}

func main() {
	x := 5
	print("From start: ")
	println(x)
	println(&x)

	// We should affect the value
	one(x)

	print("After set: ")
	fmt.Println(x) // x est
}
