# Tests

## Tests unitaires

Go inclue une fonctionnalité simple qui nous permet de faire des tests, dans le dossier math que l'on a créé dans le chapitre des modules, ajoutons-y un fichier math_test.go.

Il devra importer le module "testing" et vous devrez créer à l'intérieur un test, par exemple:

```go
func TestMoyenne(t *testing.T) {
  v := Moyenne([]float64{1,2})
  if v != 1.5 {
    t.Error("Attendu 1.5, Obtenu ", v)
  }
}
```

L'exécution pourra tester en utilisant la commande `go test`.

Notez que le nommage est important, Go ne va tester que les fichiers qui terminent par "_test.go" et exécuter seulement les fonction qui débutent par "Test". La majuscule est importante.

Il peut être logique de tester plusieurs possibilités avec un ensemble de valeurs:

```go
type paires struct {
  valeurs []float64
  moyenne float64
}

var tests = []paires{
  {[]float64{1, 2}, 1.5},
  {[]float64{1, 1, 1, 1, 1, 1}, 1},
  {[]float64{-1, 1}, 0},
  {[]float64{0}, 0},
}

func TestMoyenne(t *testing.T) {
  for _, paire := range tests {
    v := Moyenne(paire.valeurs)
    if v != paire.moyenne {
       t.Error("Pour", paire.moyenne, "attendu ", paire.moyenne, "reçu ", v)
    }
  }
}
```

Comme vous le voyez ici, il est possible de tester un ensemble de données pour s'assurer que notre test fonctionne tout le temps. Chaque structure contient un ensemble de données et le résultat anticipé.

## Benchmarks

Il est également possible de tester la vitesse d'exécution de notre code par l'entremise du module testing de go.

Soit la fonction fibonacci récursive que nous avons vu précédemment:

```go
package fib

func Fib(n int) int {
  if n < 2 {
    return n
  }
  return Fib(n-1) + Fib(n-2)
}
```

Il est possible de calculer son temps d'exécution sur plusieurs itérations, créons le fichier fib_test.go:

```go
package fib

import "testing"

func BenchmarkFib10(b *testing.B) {
// exécute la suite de fibonacci N fois 
  for n := 0; n < b.N; n++ {
    Fib(10)
  }
}
```

Notez que le nom de la fonction doit commencer par "Benchmark".
Pour exécuter le test:

`go test -bench=.`

Le "." veut dire que Go va décider lui-même du nombre de fois qu'il doit exécuter la fonction. Nous pourrions mettre un nombre au lieu du point. Go va faire la moyenne de temps de chaque exécution pour avoir un temps réaliste.

Afin de rendre le test un peu plus actuel, nous pouvons même tester plusieurs valeurs à l'intérieur de notre test (plusieurs méthodes finalement) :

```go
func benchmarkFib(i int, b *testing.B) {
  for n := 0; n < b.N; n++ {
     Fib(i)
  }
}
func BenchmarkFib1(b *testing.B)  { benchmarkFib(1, b) }
func BenchmarkFib2(b *testing.B)  { benchmarkFib(2, b) }
func BenchmarkFib3(b *testing.B)  { benchmarkFib(3, b) }
func BenchmarkFib10(b *testing.B) { benchmarkFib(10, b) }
func BenchmarkFib20(b *testing.B) { benchmarkFib(20, b) }
func BenchmarkFib40(b *testing.B) { benchmarkFib(40, b) }
```

![](img/test1.png)

Comme vous le constatez, il sera ainsi possible d'optimiser et de voir l'impact de nos modifications, vos tests et vos benchmarks pourront être à même le même fichier et pourront être testés aisément. Finalement pour éviter l'optimisation fait par un compilateur (et éviter l'exécution d'une méthode par optimisation du processeur), conservez l'exécution de vos benchmarks dans une variable:

```go
var resultat int
func benchmarkFib(i int, b *testing.B) {
    var r int
    for n := 0; n < b.N; n++ {
        r = Fib(i)
    }
    resultat = r
}
```

Ainsi il va répéter plusieurs fois le benchmark et stocker à chaque fois la valeur dans la variable resultat globale.

![](img/test2.png)