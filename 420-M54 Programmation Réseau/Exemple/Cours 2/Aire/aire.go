package main

import (
	"fmt"
	"math"
)

func rectangleAire(x1, y1, x2, y2 float64) float64 {
	l := x2 - x1
	w := y2 - y1
	return l * w
}

func cercleAire(x, y, r float64) float64 {
	return math.Pi * r * r
}

func main() {
	var rx1, ry1 float64 = 0, 0
	var rx2, ry2 float64 = 10, 10
	var cx, cy, cr float64 = 0, 0, 5
	fmt.Println(rectangleAire(rx1, ry1, rx2, ry2))
	fmt.Println(cercleAire(cx, cy, cr))
}
