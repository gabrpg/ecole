data class Auto (
  var couleur : String,
  var marque : String,
  var modèle : String ){}

fun main(){
	var bolt = Auto("jaune", "chevrolet", "bolt")
    var equinox = Auto("jaune", "chevrolet", "equinox")
    var bolt2 = Auto("jaune", "chevrolet", "bolt")
    
    if (bolt == bolt2) println("Même valeur")    
    if (bolt === bolt2) println("Même objet")
}