package main

import (
	"fmt"
)

func main() {
	elements := make(map[int]int)

	elements[1] = 5
	elements[2] = 10

	el := elements[3]
	fmt.Printf("%T", el)

}
