package main

import (
	"io"
	"net/http"
)

func main() {
	response, err := http.Get("http://example.com/")

	if err != nil {
		print("erreur")
	}

	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)

	if err != nil {
		print("erreur")
	}

	print(string(body))
}
