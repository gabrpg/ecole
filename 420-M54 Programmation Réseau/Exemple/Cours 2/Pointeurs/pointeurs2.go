package main

import (
	"fmt"
)

func one(abc *int) {
	*abc = 1
	print("From one: ")
	println(abc)  // Show memory location
	println(*abc) // Show value
}

func main() {
	x := 5
	print("From start: ")
	println(&x) // Show memory location
	println(x)  // Show value

	one(&x)
	fmt.Println(x) // x est ???
}
