package main

import (
	"fmt"
	"math"
)

type Cercle struct {
	x, y, r float64
}

func cercleAire(c *Cercle) float64 {
	return math.Pi * c.r * c.r
}

func main() {
	c := &Cercle{0, 0, 5}
	fmt.Println(cercleAire(c))
}
