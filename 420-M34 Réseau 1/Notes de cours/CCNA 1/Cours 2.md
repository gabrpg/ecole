# Systèmes d'exploitation

Tous les périphériques finaux et réseau requièrent un système d'exploitation (SE). Comme le montre la figure, la partie du OS directement liée au matériel informatique s'appelle le noyau. La partie liée aux applications et à l'utilisateur s'appelle l'interpréteur de commandes. L'utilisateur accède à l'interpréteur de commandes à l'aide d'une interface en ligne de commande (CLI) ou d'une interface utilisateur graphique.

![](images/Noyau.png)

```
analyst@secOps ~]$ ls
Desktop Downloads lab.support.files second_drive
[analyst@secOps ~]$ 
```

## Objectif du CLI

Un système d'exploitation réseau utilisant une CLI (comme Cisco IOS, installé sur un commutateur ou un routeur) permet à un technicien réseau d'effectuer les étapes suivantes :

* d'utiliser un clavier pour exécuter des programmes réseau basés sur CLI
* d'utiliser un clavier pour entrer des commandes textuelles une à la suite de l'autre. 

Vous pouvez garder votre configuration de votre appareil IOS dans un fichier texte sur votre ordinateur et la copier / coller.

Les périphériques réseau exécutent des versions spécifiques de Cisco IOS. La version de l'IOS dépend du type de périphérique utilisé et des fonctions nécessaires. Alors que tous les périphériques possèdent un IOS et un ensemble de fonctionnalités par défaut, il est possible de mettre à niveau l'IOS ou l'ensemble de fonctionnalités, afin d'obtenir des fonctions supplémentaires.

Méthodes d'accès
Un commutateur transmet le trafic par défaut et n'a pas besoin d'être explicitement configuré pour fonctionner. Par exemple, deux hôtes configurés connectés au même nouveau commutateur seraient en mesure de communiquer.

Quel que soit le comportement par défaut d'un nouveau commutateur, tous les commutateurs doivent être configurés et sécurisés.

# Méthode d'accès

## Console

Il s'agit d'un port de gestion permettant un accès hors réseau à un périphérique Cisco. L'accès hors bande désigne l'accès via un canal de gestion dédié qui est utilisé uniquement pour la maintenance des périphériques. 

L'avantage d'utiliser un port de console est que le périphérique est accessible même si aucun service réseau n'a été configuré, par exemple en effectuant la configuration initiale du périphérique réseau. Un ordinateur exécutant un logiciel d'émulation de terminal et un câble de console spécial pour se connecter à l'appareil sont nécessaires pour une connexion à la console.

![](images/port_console.webp)

## SSH (Secure Shell)

SSH est un moyen d'établir à distance une connexion CLI sécurisée via une interface virtuelle sur un réseau. À la différence des connexions de console, les connexions SSH requièrent des services réseau actifs sur le périphérique, notamment une interface active possédant une adresse. 

La pluPartie des versions de Cisco IOS incluent un serveur SSH et un client SSH qui peuvent être utilisés pour établir des sessions SSH avec d'autres périphériques.

## Telnet 

C'est un moyen non sécurisé d'établir une session CLI à distance via une interface virtuelle sur un réseau. 

Contrairement à SSH, Telnet ne fournit pas de connexion sécurisée et cryptée et ne doit être utilisé que dans un environnement de travaux pratiques. Les informations d'authentification des utilisateurs, les mots de passe et les commandes sont envoyés sur le réseau en clair. La meilleure pratique consiste à utiliser SSH au lieu de Telnet. Cisco IOS inclut à la fois un serveur Telnet et un client Telnet.

## Logiciel d'accès

Putty (sous Windows) est généralement le logiciel recommandé pour accéder à un routeur ou à une switch à distance.

# Mode de commande

Par mesure de sécurité, le logiciel Cisco IOS sépare l'accès à la gestion en deux modes de commande:

## Mode d'exécution utilisateur

Ce mode offre des fonctionnalités limitées, mais s'avère utile pour les opérations de base. Il autorise seulement un nombre limité de commandes de surveillance de base, mais il n'autorise aucune commande susceptible de modifier la configuration du périphérique. Le mode d'exécution utilisateur se reconnaît à l'invite CLI qui se termine par le symbole >.

## Mode d'exécution privilégié 

Pour exécuter les commandes de configuration, un administrateur réseau doit accéder au mode d'exécution privilégié. Pour accéder aux modes de configuration supérieurs, comme celui de configuration globale, il est nécessaire de passer par le mode d'exécution privilégié. Le mode d'exécution privilégié se reconnaît à l'invite qui se termine par le symbole # .

## Mode de configuration

Pour configurer le périphérique, l'utilisateur doit passer en mode de configuration globale, par la commande **enable**

Les modifications de la configuration effectuées dans l'interface de ligne de commande en mode de configuration globale affectent le fonctionnement du périphérique dans son ensemble. Le mode de configuration globale se reconnaît à l'invite se terminant par (config)#.

L'accès au mode de configuration globale se fait avant les autres modes de configuration spécifiques. À partir du mode de config. globale, l'utilisateur peut accéder à différents sous-modes de configuration. Chacun de ces modes permet de configurer une partie ou une fonction spéciale du périphérique IOS. Les deux sous-modes de configuration les plus courants sont les suivants:

### Mode de configuration ligne

Utilisé pour configurer l'accès par la console, par SSH, par Telnet, ou l'accès AUX.

### Mode de configuration d'interface

Utilisé pour configurer l'interface réseau d'un port de commutateur ou d'un routeur.

# Naviguer entre les différents modes IOS

Différentes commandes sont utilisées pour entrer et sortir des invites de commandes. Pour passer du mode utilisateur au mode privilégié, utilisez la commande **enable** . Utilisez la commande **disable** du mode d'exécution privilégié pour retourner au mode d'exécution utilisateur.

Exemples:

```
Switch(config)# line console 0
Switch(config-line)# exit
Switch(config)#

Switch(config-line)# end
Switch#

Switch(config-line)# interface FastEthernet 0/1
Switch(config-if)#
```

# Commande

![](images/Commandes_IOS.png)

## Syntaxe

Convention | Description 
 --- | ---
gras	|  Le texte en gras signale les commandes et mots-clés à saisir tels quels.
Italique| Le texte en italique signale les arguments pour lesquels des valeurs doivent être saisies.
[x] |	Les crochets signalent un élément facultatif (mot-clé ou argument).
{x} |	Les accolades signalent un élément requis (mot-clé ou argument).
[x {y \| z }] |	Les accolades et les lignes verticales encadrées par des crochets signalent un choix obligatoire, au sein d'un élément facultatif. Les espaces sont utilisés pour délimiter clairement les parties de la commande.

Tapez ? pour obtenir de l'aide. 

# Noms de périphériques

Vous avez beaucoup appris sur le Cisco IOS, la navigation dans l'IOS et la structure de commande. Maintenant, vous êtes prêt à configurer les périphériques! La première commande de configuration sur un périphérique doit être l'attribution d'un nom de périphérique ou un nom d'hôte unique. Par défaut, tous les périphériques se voient attribuer un nom d'usine par défaut. Par exemple, un commutateur Cisco IOS est "Switch".

Si tous les périphériques réseau ont conservé leurs noms par défaut, il sera difficile d'identifier un périphérique spécifique. Par exemple, comment sauriez-vous que vous êtes connecté au bon appareil lorsque vous y accédez à distance à l'aide de SSH? Le nom d'hôte confirme que vous êtes connecté au périphérique approprié.

Le nom par défaut doit être remplacé par quelque chose de plus descriptif. En revanche, si vous les choisissez intelligemment, vous n'aurez aucune peine à mémoriser, décrire et utiliser les noms des périphériques réseau. Les directives pour la configuration des noms d'hôte sont répertoriées ci-dessous:

* débutent par une lettre
* Ne contiennent pas d'espaces
* se terminent par une lettre ou un chiffre
* Ne comportent que des lettres, des chiffres et des tirets
* Comportent moins de 64 caractères

```
Switch# configure terminal
Switch(config)# hostname Sw-Floor-1
Sw-Floor-1(config)#
```

# Mots de passe forts

L'utilisation de mots de passe faibles ou facilement devinés continue d'être la préoccupation la plus importante des organisations en matière de sécurité. Des mots de passe doivent toujours être configurés pour les périphériques réseau, y compris les routeurs sans fil domestiques, afin de limiter l'accès administratif.

Cisco IOS peut être configuré pour utiliser des mots de passe de mode hiérarchique afin de permettre d'établir différents privilèges d'accès à un périphérique réseau.

Tous les périphériques réseau doivent limiter l'accès administratif en sécurisant les accès d'exécution, d'exécution utilisateur et Telnet à distance avec des mots de passe. En outre, tous les mots de passe doivent être cryptés et des notifications légales doivent être fournies.

Utilisez des mots de passe forts qui ne sont pas faciles à deviner. Pour choisir les mots de passe, respectez les règles suivantes:

* Utilisez des mots de passe de plus de 8 caractères.
* Utilisez une combinaison de lettres majuscules et minuscules, des chiffres, des caractères spéciaux et/ou des séquences de chiffres.
* Évitez d'utiliser le même mot de passe pour tous les périphériques.
* N'utilisez pas des mots courants car ils sont faciles à deviner.
* Utilisez un générateur de mot de passe. ils permettront de définir la longueur, le jeu de caractères et d'autres paramètres.

https://www.lastpass.com/fr/features/password-generator

*La plupart des laboratoires de ce cours utilisent des mots de passe simples tels que cisco ou ****. Il faut éviter ces mots de passe dans les environnements de production, car ils sont considérés comme faibles et faciles à deviner. Nous n'utilisons ces mots de passe que pour une utilisation dans une salle de cours ou pour illustrer des exemples de configuration.*

## Configurer les mots de passe
Lorsque vous vous connectez initialement à un périphérique, vous êtes en mode d'exécution utilisateur. Ce mode est sécurisé à l'aide de la console.

```
Sw-Floor-1# configure terminal
Sw-Floor-1(config)# line console 0
Sw-Floor-1(config-line)# password cisco
Sw-Floor-1(config-line)# login
Sw-Floor-1(config-line)# end
Sw-Floor-1#
```

La console d'accès requiert à présent un mot de passe avant d'accéder au mode d'exécution utilisateur.

Pour disposer d'un accès administrateur à toutes les commandes IOS, y compris la configuration d'un périphérique, vous devez obtenir un accès privilégié en mode d'exécution. C'est la méthode d'accès la plus importante car elle fournit un accès complet à l'appareil.

Pour sécuriser l'accès au mode d'exécution privilégié, utilisez la commande de configuration globale **enable secret mot de passe**

```
Sw-Floor-1# configure terminal
Sw-Floor-1(config)# enable secret class
Sw-Floor-1(config)# exit
Sw-Floor-1#
```

Les lignes VTY (terminal virtuel) activent l'accès à distance au périphérique en utilisant Telnet ou SSH. Plusieurs commutateurs Cisco prennent en charge jusqu'à 16 lignes VTY, numérotées de 0 à 15.

Pour sécuriser les lignes VTY, entrez le mode VTY ligne à l'aide de la commande de config. globale line vty 0 15 . Spécifiez ensuite le mot de passe VTY à l'aide de la commande password mot de passe . En dernier lieu, activez l'accès VTY à l'aide de la commande login .

```
Sw-Floor-1# configure terminal
Sw-Floor-1(config)# line vty 0 15
Sw-Floor-1(config-line)# password cisco 
Sw-Floor-1(config-line)# login 
Sw-Floor-1(config-line)# end
Sw-Floor-1#
```

## Chiffrer les mots de passe pour l'affichage
Les fichiers startup-config et running-config affichent la pluPartie des mots de passe en clair. C'est une menace à la sécurité dans la mesure où n'importe quel utilisateur peut voir les mots de passe utilisés s'il a accès à ces fichiers.

```
Sw-Floor-1# configure terminal
Sw-Floor-1(config)# service password-encryption
Sw-Floor-1(config)#
```

La commande applique un chiffrement simple à tous les mots de passe non chiffrés. Ce chiffrement ne s'applique qu'aux mots de passe du fichier de configuration; il ne s'applique pas lorsque les mots de passe sont transmis sur le réseau. Le but de cette commande est d'empêcher les personnes non autorisées de lire les mots de passe dans le fichier de configuration.

Utilisez la commande show running-config pour vérifier que les mots de passe sont maintenant chiffrés.

```
Sw-Floor-1# show running-config
!

!
line con 0
password 7 094F471A1A0A 
login
!
line vty 0 4
password 7 03095A0F034F38435B49150A1819
login
!
!
end
```

# Messages de bannière

Bien que les mots de passe soient l'un des moyens dont vous disposez pour empêcher l'accès non autorisé à un réseau, il est vital de mettre en place une méthode pour déclarer que l'accès à un périphérique est réservé aux seules personnes autorisées. À cet effet, ajoutez une bannière aux informations affichées par le périphérique. Les bannières peuvent constituer une pièce importante dans un procès intenté à une personne qui aurait accédé illégalement à un périphérique. En effet, dans certains systèmes juridiques, il est impossible de poursuivre des utilisateurs, ni même de les surveiller s'ils n'ont pas reçu de notification appropriée au préalable.

Pour créer une bannière MOTD (Message Of The Day) sur un périphérique réseau, utilisez la commande de config. globale du banner motd # du message du jour # . Le "#" situé dans la syntaxe de la commande est le caractère de délimitation. Il est placé avant et après le message. Vous pouvez utiliser comme délimiteur tout caractère ne figurant pas dans le message. C'est la raison pour laquelle les symboles tels que "#" sont souvent employés comme délimiteurs. Une fois cette commande exécutée, la bannière s'affiche lors de toutes les tentatives d'accès au périphérique jusqu'à ce que vous la supprimiez.

L'exemple suivant montre les étapes de configuration de la bannière sur Sw-Floor-1.

```
Sw-Floor-1# configure terminal
Sw-Floor-1(config)# banner motd #Authorized Access Only#
```

# Fichiers de configuration

Deux fichiers système stockent la configuration des périphériques:

## startup-config 

Ceci est le fichier de configuration enregistré qui est stocké dans NVRAM. Ce fichier stocké dans la mémoire vive non volatile contient toutes les commandes qui seront utilisées au démarrage ou au redémarrage. La mémoire vive non volatile ne perd pas son contenu lors de la mise hors tension du périphérique.

## running-config 

Ceci est stocké dans la mémoire vive (RAM). Il reflète la configuration actuelle. Modifier une configuration en cours affecte immédiatement le fonctionnement d'un périphérique Cisco. La RAM est une mémoire volatile. Elle perd tout son contenu lorsque le périphérique est mis hors tension ou redémarré.

La commande mode d'exécution privilégié show running-config est utilisée pour afficher la configuration en cours d'exécution. Comme indiqué dans l'exemple, la commande répertorie la configuration complète actuellement stockée dans la RAM.

```
Sw-Floor-1# show running-config
Building configuration...
Current configuration : 1351 bytes
!
! Last configuration change at 00:01:20 UTC Mon Mar 1 1993
!
version 15.0
no service pad
service timestamps debug datetime msec
service timestamps log datetime msec
service password-encryption
!
hostname Sw-Floor-1
!
(output omitted)
```

Pour afficher le fichier de configuration initiale, lancez la commande show startup-config du mode d'exécution privilégié.

### En cas de panne de courant ou de redémarrage du périphérique

Toutes les modifications de la configuration que vous n'avez pas enregistrées sont perdues. Pour enregistrer les modifications apportées à la configuration en cours dans le fichier de configuration initiale, utilisez la commande **copy running-config startup-config** du mode d'exécution privilégié.

### Modifier la configuration en cours

Vous pouvez restaurer l'appareil dans sa configuration précédente. Supprimez les commandes modifiées individuellement ou rechargez le périphérique à l'aide de la commande en mode d’exécution reload privilégié pour restaurer la configuration de démarrage.

L'inconvénient de l'utilisation de la commande reload pour supprimer une configuration en cours non enregistrée est le court délai durant lequel le périphérique est hors ligne, entraînant une panne de réseau.

Par ailleurs, si des modifications indésirables ont été enregistrées dans la configuration initiale, il peut s'avérer nécessaire de supprimer toutes les configurations. Pour ce faire, vous devez effacer la configuration initiale et redémarrer le périphérique. La commande **erase startup-config** du mode d'exécution privilégié permet de supprimer la configuration initiale. Quand vous entrez cette commande, le commutateur vous demande de la confirmer. Appuyez sur Enter pour accepter la connexion.

Après avoir supprimé le fichier de configuration initiale de la mémoire NVRAM, rechargez le périphérique pour supprimer le fichier de configuration en cours de la mémoire vive. Lors du rechargement, un commutateur charge la configuration initiale qui était proposée à l'origine avec le périphérique.

# Adresses IP

Si vous souhaitez que vos périphériques finaux communiquent entre eux, vous devez vous assurer que chacun d'eux possède une adresse IP appropriée et qu'il est correctement connecté. Vous en apprendrez plus sur les adresses IP, les ports de périphériques et les supports utilisés pour connecter les périphériques dans cette rubrique.

L'utilisation d'adresses IP est le principal moyen permettant aux périphériques de se localiser les uns les autres et d'établir la communication de bout en bout sur Internet. Chaque périphérique final d'un réseau doit être configuré avec une adresse IP:

* Ordinateurs (stations de travail, ordinateurs portables, serveurs de fichiers, serveurs web)
* Imprimantes réseau
* Téléphones VoIP
* Caméras de surveillance
* Smartphones
* Périphériques portables mobiles (par exemple, lecteurs de codes à barres sans fil)

La structure d'une adresse IPv4 est appelée «notation décimale à point» et est composée de quatre nombres décimaux compris entre 0 et 255. Les adresses IPv4 sont affectées à des périphériques individuels connectés à un réseau.

Remarque: Dans ce cours, «IP» fait référence aux protocoles IPv4 et IPv6. L'IPv6 est la version la plus récente de l'IP et remplace l'IPv4.

Avec une adresse IPv4, un masque de sous-réseau est également nécessaire. Un masque de sous-réseau IPv4 est une valeur 32 bits qui différencie la partie réseau de l'adresse de la partie hôte. Associé à l'adresse IPv4, le masque de sous-réseau détermine à quel sous-réseau spécifique le périphérique appartient:

192.168.1.10 / 255.255.255.0

Les adresses IPv6 ont une longueur de 128 bits et sont notées sous forme de chaînes de valeurs hexadécimales. Tous les groupes de 4 bits sont représentés par un caractère hexadécimal unique, pour un total de 32 valeurs hexadécimales. Les groupes de quatre chiffres hexadécimaux sont séparés par un deux-points (:). Les adresses IPv6 ne sont pas sensibles à la casse et peuvent être notées en minuscules ou en majuscules.

2001:db8:acad:10::10

# Interfaces et ports

Les communications réseau dépendent des interfaces des périphériques utilisateur final, des interfaces des périphériques réseau et des câbles de connexion. Chaque interface a des caractéristiques, ou des normes, qui la définissent. Un câble de connexion à l'interface doit donc être adapté aux normes physiques de l'interface. Ces supports de transmission peuvent être des câbles en cuivre à paires torsadées, des câbles à fibres optiques, des câbles coaxiaux ou une liaison sans fil, comme illustré dans la figure.

Les différents types de supports réseau possèdent divers avantages et fonctionnalités. Tous les supports réseau n'ont pas les mêmes caractéristiques. Tous les médias ne sont pas appropriés pour le même objectif Les différences entre les types de supports de transmission incluent, entre autres:

* la distance sur laquelle les supports peuvent transporter correctement un signal
* l'environnement dans lequel les supports doivent être installés
* la quantité de données et le débit de la transmission
* le coût des supports et de l'installation.

Chaque liaison à Internet requiert un type de support réseau spécifique, ainsi qu'une technologie réseau particulière. Par exemple, l'Ethernet est la technologie de réseau local (LAN) la plus répandue aujourd'hui. Les ports Ethernet sont présents sur les périphériques des utilisateurs finaux, les commutateurs et d'autres périphériques réseau pouvant se connecter physiquement au réseau à l'aide d'un câble.

Les commutateurs Cisco IOS de couche 2 sont équipés de ports physiques pour permettre à des périphériques de s'y connecter. Ces ports ne prennent pas en charge les adresses IP de couche 3. Par conséquent, les commutateurs ont une ou plusieurs interfaces de commutateur virtuelles (SVI). Ces interfaces sont virtuelles car il n'existe aucun matériel sur le périphérique associé. Une interface SVI est créée au niveau logiciel.

L'interface virtuelle est un moyen de gérer à distance un commutateur sur un réseau grâce à l'IPv4 et l'IPv6 Chaque commutateur dispose d'une interface SVI apparaissant dans la configuration par défaut prête à l'emploi. L'interface SVI par défaut est l'interface VLAN1.

**Un commutateur de couche 2 ne nécessite pas d'adresse IP. L'adresse IP attribuée à l'interface SVI sert à accéder à distance au commutateur. Une adresse IP n'est pas nécessaire pour permettre au commutateur d'accomplir ses tâches.**

## Configuration de l'interface de commutateur virtuelle

Pour accéder à distance au commutateur, une adresse IP et un masque de sous-réseau doivent être configurés sur l'interface SVI. Pour configurer une SVI sur un commutateur, utilisez la commande de configuration globale interface vlan 1 . Vlan 1 n'est pas une interface physique réelle mais une interface virtuelle. Attribuez ensuite une adresse IPv4 à l'aide de la commande de configuration d'interface **ip address ip-address subnet-mask**. Enfin, activez l'interface virtuelle à l'aide de la commande de configuration d'interface **no shutdown**.

Une fois ces commandes configurées, le commutateur dispose de tous les éléments IPv4 adaptés pour la communication sur le réseau.

```
Sw-Floor-1# configure terminal
Sw-Floor-1(config)# interface vlan 1
Sw-Floor-1(config-if)# ip address 192.168.1.20 255.255.255.0
Sw-Floor-1(config-if)# no shutdown
Sw-Floor-1(config-if)# exit
Sw-Floor-1(config)# ip default-gateway 192.168.1.1
```

# Todo 

2.1.6

2.2.8

2.4.7

2.4.8

2.6.3

2.7.6

