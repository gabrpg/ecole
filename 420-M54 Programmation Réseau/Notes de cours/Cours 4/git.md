# GIT

La première étape consiste à créer un compte sur gitlab. J'ai choisi gitlab car il est plus simple de faire des repository privé.


## Repository 

Par la suite, vous devrez créer un repository. Un repository représente un ensemble de répertoires et de fichiers appartenant généralement à un projet. 

Il peut représenter un seul projet open source tel que celui-ci:

https://github.com/home-assistant/core

Ou sinon en représenter plusieurs tel que mon repository qui servira aux notes de cours de tous mes cours:

https://gitlab.com/drynish/ecole

## Accès

Vous avez deux choix pour vous connecter à distance:

* Login / mot de passe (insécure)
* Clé privée / public

### Génération de clé privée (à valider)

``ssh-keygen``

Par défaut, les clés vont se générer dans le répertoire P:\.ssh

Copier la clé publique sur le site de Gitlab ou GitHub dans le profil sous ssh keys.

Ceci va vous permettre de vous identifier, seulement vous, vous n'aurez pas à rentrer de nouveau votre mot de passe.

Vous devrez transporter votre clé privée avec vous ou vous en créer de nouvelles et les téléverser (par exemple pour votre ordinateur à la maison).

De mon côté, je prends la même.

### git clone

Partir d'un répository existant: 

``git clone git@gitlab.com/drynish/ecole"````

va faire que vous allez faire une copie du repository ecole dans votre répertoire en cours.

Vous pourrez cloner le repository que vous avez créer précédemment

### git init

On crée un repository local

Qu'on pourra modifier ou le pousser par l'entremise de

## git remote

Permet de modifier l'origine du repository (l'endroit ou on pousse nos modifications)

``git remote set-url origin new.git.url/here``

### git add

On ajoute des éléments à git à surveiller dans notre arbre local.

``git add nom_de_fichier ou répertoire``

### git commit

* On ajoute une version locale de notre code. Sachez qu'on peut toujours revenir en arrière en utilisant checkout.

* On ne commit jamais du code non-fonctionnel.

* On commit souvent, à chaque fin de création d'une méthode. On ne sait jamais à quel moment on va induire un bug qui ne pourra être résolu qu'en revenant en arrière.

``git commit -m "Nom de la nouvelle version"``

### git push

On pousse notre version actuelle sur le serveur à distance.

``git push``

### git pull

On obtient la dernière version du serveur à distance sur l'ordinateur local. **Fonctionne uniquement s'il y a un repository déjà configuré dans le répertoire en cours**.

``git pull``


