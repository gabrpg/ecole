package main

import (
	"flag"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
)

type SortTypeFlag struct{ SortType []string }

func (s *SortTypeFlag) GetAlgoNames() []string {
	return s.SortType
}

func (s *SortTypeFlag) String() string {
	return fmt.Sprint(s.SortType)
}

func (s *SortTypeFlag) Set(v string) error {
	if len(s.SortType) > 0 {
		return fmt.Errorf("Ne pas utiliser un type plus qu'une fois")
	}
	names := strings.Split(v, ",")
	s.SortType = append(s.SortType, names...)
	return nil
}

func BubbleSort(elements []int) []int {
	for i := 0; i < len(elements)-1; i++ {
		for j := 0; j < len(elements)-1; j++ {
			if elements[j] > elements[j+1] {
				elements[j+1], elements[j] = elements[j], elements[j+1]
			}
		}
	}
	return elements
}

func QuickSort(elements []int) []int {
	if len(elements) < 2 {
		return elements
	}
	l, r := 0, len(elements)-1
	pivot := rand.Int() % len(elements)
	elements[pivot], elements[r] = elements[r], elements[pivot]

	for i, _ := range elements {
		if elements[i] < elements[r] {
			elements[l], elements[i] = elements[i], elements[l]
			l++
		}
	}

	elements[l], elements[r] = elements[r], elements[l]

	QuickSort(elements[:l])
	QuickSort(elements[l+1:])

	return elements
}

func SelectionSort(elements []int) []int {
	size := len(elements)
	var mindex int
	for i := 0; i < size-1; i++ {
		mindex = i
		for j := i + 1; j < size; j++ {
			if elements[j] < elements[mindex] {
				mindex = j
			}
		}
		elements[i], elements[mindex] = elements[mindex], elements[i]
	}
	return elements
}

func main() {
	var sorting SortTypeFlag

	flag.Var(&sorting, "sort", "exemple: -sort=quick,bubble 88 33 99 55")
	flag.Parse()

	intArr := make([]int, len(flag.Args()))
	for index, val := range flag.Args() {
		intArr[index], _ = strconv.Atoi(val)
	}

	for _, item := range sorting.GetAlgoNames() {

		switch item {
		case "quick":
			fmt.Println("Quick Sort:", QuickSort(intArr))
		case "select":
			fmt.Println("Selection Sort:", SelectionSort(intArr))
		case "bubble":
			fmt.Println("Bubble Sort:", BubbleSort(intArr))
		}
	}

}
