## Array

Comme dans tous les langages de programmation, le tableau permet de stocker un ensemble d'éléments de type identique à l'intérieur.

```go
var t [3]int 
a[0] = 4
a[1] = 5
```
ou plus simplement
```go
t := [3]int{4,5}
```

Les arrays sont définis avec leur taille, donc ils sont fixes (ne peuvent donc pas être redimensionnés).

## Slice

Un slice est un sous-ensemble d'un array, par contre, il est dynamique dans le sens qu'on n'a pas besoin de spécifier sa taille lors de la création:

```go
t := [6]int{4,5,2,54,12,34}
var s []int = t[1:3]
```

C'est un range à demi ouvert, il inclut le premier, mais n'inclut pas le dernier. Donc dans ce cas-ci, on serait porté à croire 1 à 3, mais en réalité, ce n'est que 1 à 2:

```
s = [5 2]
```

Les slices ne contiennent aucune donnée, ce sont en fait que des références vers les données d'un tableau. Il s'agit presque d'un résumé.

Donc, comme vous vous en doutez, en modifiant les données d'un slice, vous allez modifier les données du tableau:

```go
noms:= [4]string{
"Michel",
"Philippe",
"Charles",
"France",
}

fmt.Println(noms)
a := noms[0:2] // Contient Michel et Philippe
b := noms[1:3] // Contient Philippe et Charles
b[0] = "XXX" // Remplace Philippe par XXX
fmt.Println(a, b)
fmt.Println(noms)
// a = Michel XXX
// b = XXX Charles
// noms = Michel XXX Charles France 
```
https://go.dev/play/p/ipd5vYeLXbH



## Structure de contrôle

### if

Le if ne doit pas être encadré de parenthèses, par contre les différentes sections devront être encadrées d'accolades comme en C.

```go
func main() {
    if 7%2 == 0 {
        fmt.Println("7 est pair")
    } else {
        fmt.Println("7 est impair")
    }
    if 8%4 == 0 {
        fmt.Println("8 est divisible par 4")
    }
    if num := 9; num < 0 {
        fmt.Println(num, "est négatif")
    } else if num < 10 {
        fmt.Println(num, "n'a qu'un chiffre")
    } else {
        fmt.Println(num, "a plusieurs chiffres")
    }
}
```

Le else est exactement comme en C. Il peut être présent ou non. Veuillez remarquer le calcul dans la dernière possible, il est possible de faire une opération dans le if. Notez également que les paranthères ne sont pas nécessaires, mais les accolades oui!

### switch

Il est vraiment identique au C également, cependant le break n'est pas nécessaire. Il ne va qu'exécuter que le case nécessaire.

```go
func main() {
	fmt.Print("Go runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		// freebsd, openbsd,
		// plan9, windows...
		fmt.Printf("%s.\n", os)
	}
}
```

Une autre différence notable c'est qu'on peut faire un switch sur à peu près tous les types possibles:

```go
t := time.Now()
switch {
case t.Hour() < 12:
    fmt.Println("Avant-midi")
default:
    fmt.Println("Après-dîner")
}
```

## Boucles

Go n'a qu'un seul élément pour les boucles, le for.

Les 3 parties du for (comme vous vous en doutez):

- init (optionnel)
- condition (optionnel)
- post (optionnel)

Voici un exemple de déclaration:

```go
func main() {
	somme := 0
	for i := 0; i < 10; i++ {
		somme += i
	}
	fmt.Println(somme)
}
```

En go, contrairement en C/C++, il n'y a pas de parenthèses après la déclaration du for. La présence des accolades est obligatoire (bien sûr). L'itération de la boucle va se terminer aussitôt que la condition i<10 sera fausse.

Lorsque le *init* et le *post* sont retirés, vous obtenez le while (vous pouvez même retirer les ;) :

```go
func main() {
	somme := 1
	for somme < 1000 {
		somme += somme
	}
	fmt.Println(somme)
}
```

Si finalement vous retirez les 3 paramètres du for, vous obtenez alors une boucle infinie:

```go
package main

func main() {
    for {
    }
}
```

### Range

Le range permet de faire une itération sur un tableau ou un slice. Très utile pour boucler sur chacun des éléments sans être obligé d'ajouter une structure traditionnelle. Les 2 boucles suivantes font la même chose:

```go
for i := 0 ; i <= len(tab); i++ {
    // i comme indice et tab[i] comme valeur
}
for i, v := range tab {
   // i comme indice et v comme valeur
}
```

Par exemple:
```go
var pow = []int{1, 2, 4, 8, 16, 32, 64, 128}
func main() {
	for i, v := range pow {
		fmt.Printf("2**%d = %d\n", i, v)
	}
}
```

Si vous n'avez pas besoin des indices:

```go
for _, v := range pow {
    fmt.Printf("%d\n", v)
}
```

