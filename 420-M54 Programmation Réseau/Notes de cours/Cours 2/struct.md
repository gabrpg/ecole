# Struct et interface

## Struct

Imaginons la situation suivante: vous avez à calculer l'aire de différents objets:

```go
func rectangleAire(x1, y1, x2, y2 float64) float64 {
    b := x2 - x1
    h := y2 - y1
    return b * h
}

func cercleAire(x, y, r float64) float64 {
    return math.Pi * r * r
}

func main() {      
    var rx1, ry1 float64 = 0, 0
    var rx2, ry2 float64 = 10, 10
    var cx, cy, cr float64 = 0, 0, 5
    fmt.Println(rectangleAire(rx1, ry1, rx2, ry2))
    fmt.Println(cercleAire(cx, cy, cr))
}
```

[Exemple](../../Exemple/Cours%202/Aire/)

Bien que ceci est fonctionnel, il est difficile d'avoir une certaine homogéniété, surtout si l'on commence à ajouter des structures différentes (tel que triangle et trapèze).

On va premièrement se créer des types d'objets qui nous permettront alors d'avoir une plus grande précision dans nos objets (pas seulement par le nom des méthodes):

```go
type Cercle struct {
    x, y, r float64 
}

func cercleAire(c Cercle) float64 {
    return math.Pi * c.r * c.r
}

func main() {
    c := Cercle{0, 0, 5}
    fmt.Println(cercleAire(c))
}
```

Comme vous pouvez le voir, il est possible de se créer une structure Cercle et d'utiliser cette méthode à même nos méthodes afin de pouvoir utiliser les champs à l'intérieur de notre méthode C.

Comme vu précédemment, les paramètres sont toujours des objets copiés, il est généralement recommandé d'utiliser des pointeurs vers nos objets, il est recommandé d'utiliser un pointeur vers l'objet en paramètre:

```go
func cercleAire(c *Cercle) float64 {
    return math.Pi * c.r * c.r
} 

func main() {
    c := Cercle{0, 0, 5}
    fmt.Println(cercleAire(&c))
}
```

[Exemple](../../Exemple/Cours%202/Aire%202/)

Une autre particularité du code est de voir la création de l'objet cercle. Il y a 4 façons possibles de créer cet objet:

`var c Cercle` : permet d'obtenir un objet Cercle avec les champs égaux à 0 (x, y, r)

`c := new Cercle` : permet d'obtenir un pointeur vers un objet Cercle avec les champs égaux à 0 (x, y, r)

`c := Cercle { x:0 , y:0, r:5 }` permet d'obtenir un objet Cercle avec les champs égaux à ( x:0, y:0, r:5)

`c := Cercle { 0, 0, 5}` permet d'obtenir un objet Cercle avec les champs définis en ordre (x, y, r) tel que défini dans le type

## Méthode

Afin de créer une méthode d'une structure, nous devons utiliser la syntaxe suivante:

```go
func (c *Cercle) aire() float64 {
    return math.Pi * c.r * c.r
}

func main() {
    c := Cercle{0, 0, 5}
    fmt.Println(c.aire())
}
```

Donc désormais nous avons une méthode qui a été rajoutée à l'objet Cercle, faisons de même pour l'objet rectangle:

```go
type Rectangle struct {
    x1, y1, x2, y2 float64
}

func (r *Rectangle) aire() float64 {
    return (r.x2 - r.x1) * (r.y2 - r.y1)
}
```

## Interface

Nous avons nommé les deux méthodes des type créés aire, c'est loin d'être un hasard. Une relation comme ça entre deux objets est très fréquente dans la vraie vie. Enfin de standardiser ce genre de relation, Go a une façon de faire très simple:

```go
type Forme interface {
    aire() float64
}
```

Ceci, de façon totalement magique, indique que Forme est une interface, pour être donc considéré comme une Forme, vous devez implémenter la méthode aire. Et c'est tout... À partir de maintenant, les deux objets créés précédemment sont des Formes (car les deux implémentent la méthode aire)

Nous pouvons créer une méthode aireTotale, pour calculer, la superficie totale de tous les objets. Nous pourrions faire ceci:

```go
func aireTotale(cercles... Cercle, rectangles Rectangle float64) {
    var total float64
    for _, c := range cercles {
        total += c.aire()
    }

    for _, r := range rectangles {
        total += r.aire()
    }
    return total
}
```

[Exemple](../../Exemple/Cours%202/Aire%203/)

Est-ce que c'est bon? Oui. Est-ce que c'est maintenable? Non. Pourquoi? Car si vous créez de nouvelles Formes, il faudra modifier cette méthode à chaque fois pour ajouter tous les types possibles... 

Mais utilisons la propriété des Formes pour sauver nos vies (vu que oui, on travaille avec plein d'objets supportant les formes (vous vous rappelez les interfaces) :

```go
func aireTotale(formes ...Forme) float64 {
    var total float64
    for _, f := range formes {
        total += f.aire()
    }
    return total
}
```

Et voilà, tant qu'un objet est une Forme, nous pouvons utiliser leur propriété aire, pour calculer leur aire. Ainsi en ajoutant de nouveaux objets tant que vous ajoutez les méthodes aires à chacun des objets, vous n'aurez rien d'autre à faire et votre fonction aireTotale fonctionnera.

Nous pouvons utiliser les Interface comme des membre d'une structure, ceci permet de créer une liste de forme par exemple.

```go
type MultiForme struct {
    formes []Forme
}
```

Ceci définit donc une structure qui contient des objets implémentant la méthode aire. Nous pourrions aussi créer la méthode aire dans cette structure avec qu'elle soit une Forme en lui ajoutant la fameuse méthode aire:

```go
func (m MultiForme) aire() float64 {
    var total float64
    for _, f := range m.formes {
        total += f.aire()
    }

    return total
}
```

Et dans le main:

```go
multiForme := MultiForme {
    formes : [] Forme {
        Cercle{0, 0, 5},
        Rectangle{0, 0, 10, 10},
    },
}

fmt.Println(multiForme.aire())
```

[Exemple](../../Exemple/Cours%202/Aire%204/)

C'est magique!