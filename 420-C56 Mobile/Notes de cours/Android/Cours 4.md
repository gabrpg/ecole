# Les applications Compose

Quand Android a été inventé il y a 10 ans, il a gagné beaucoup de popularité chez les développeurs, car c'était très simple d'écrire des applications. Vous deviez développer une interface dans un fichier XML et la connecter à une Activity. Ceci fonctionnait plutôt bien, car les applications étaient relativement simples et les développeurs devaient supporter qu'un nombre limité d'appareils.

Depuis ce temps, plusieurs choses ont changé. Avec chaque nouvelle plateforme Android, on gagnait davantage de possibilités. Les manufacturiers ont créé des appareils avec des grandeurs d'écran, des densités de pixels et des formes différentes. Google a fait de leur mieux pour garder le système de vue compréhensible, mais la complexité des applications devenait de plus en plus ardue: l'utilisation de liste déroulante requérant beaucoup de code passe-partout répétitif (boiler code).

Ceci n'était pas spécifique à Android, d'autres plateformes se sont retrouvées avec les mêmes problèmes. Les problèmes proviennent principalement du fait que les librairies d'interface utilisaient l'approche impérative. Il fallait utiliser un changement de paradigme de développement. Le cadriciel React a été un des premiers à populariser l'approche déclarative. Les autres plateformes (SwiftUI et Flutter) ont suivi.

Jetpack Compose est l'interface déclarative de l'interface d'Android. 

## Premier exemple

Les fonctions composables peuvent être facilement identifiées par l'annotation @Composable. Elles émettent un élément d’interface. 

```
@Composable
fun Welcome() {
    Text(
        text = stringResource(id = R.string.welcome),
        style = MaterialTheme.typography.subtitle1
    )
}
```

Android va inclure par défaut:

```
import androidx.compose.material3.Text
```

Afin d'utiliser Text ou tout autre éléments Material Design, le fichier build.gradle doit contenir la phrase suivante:

```
implementation("androidx.compose.material3:material3:1.1.1")
```

N'oubliez pas de synchroniser si vous modifiez quoi que ce soit dans le fichier gradle.

Le composable Text dans Welcome() est configuré avec deux paramètres: 
* text : contenu (r.string pointe aux ressources). stringResource set une fonction prédéfinie appartenant à androidx.compose.ui.res.
* style: modifie l'apparence de votre fonction composable.

## Preview

Pour pouvoir voir de quoi notre code à l'air, vous pouvez utiliser @Preview devant vos méthodes, comme ça vous allez pouvoir voir avant la compilation de quoi à l'air votre rendu.

## Activity

Template de base dans une activité:

```kotlin
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Hello()
                }
            }
        }
    }
}
```

Pour qu'une activité puisse créer des interfaces composables,  votre activité doit extensionnée ComponentActivity ou une de ses descendances. C'est le cas pour androidx.fragment.app.FragmentActivity et androidx.appcompat.app.AppCompatActivity.

Une grande différence entre compose et view, compose appelle la méthode setContent() alors que les view appelle setContentView. Si l'on compare les deux méthodes, il n'y a aucune façon de référencer des objets dans l'arbre des UI avec compose.

## Impératif

* findViewById<Type>(R.id.objet)
* binding
* défini dans un fichier XML
* à l'exécution, transposé en un arbre de composant 
* pour changer l'interface utilisateur, tous les composants doivent être modifiés et redessinés.

## Déclaratif

* Le point d'entrée d'un item UI Composable est une fonction Composable
* De là d'autre fonction composable peuvent être appelée
* L'ordre des controles est définis par la fonction Composable

[Exemple](../../Exemples/Android/Cours%204/Compose)

La fonction Factorial() définit deux objets:
* expanded
* text

Une fonction layout définit l'état initial des objets alors que les objets composables définissent l'état actuel des objets, nous n'avons pas besoin d'établir des états initiaux. Lorsqu'ils doivent être mis à jour, les objets d'interface doivent être manipulés par le setValue.

Les fonctions composables ont des portions obligatoires et des portions optionnelles, ils restent ainsi jusqu'à ce que leur propriété soient modifiés par le changement de valeur de leur propriété. Comment mettre à jour un Composant UI?

Le processus se nomme recomposition et il s'exécute automatiquement lorsqu'un élément du UI doit être mis à jour. Si vous passez toujours le même texte à un composant UI Text(), il ne sera jamais redessiné. Si un Composant qui peut être modifiable, il sera redessiné lorsque nécessaire. Une valeur qui change avec le temps dans un composant Compose est nommé **state**. On peut créer un état en utilisant la commande mutableStateOf(). Pour référer à un état composable, vous devez vous rappeler de l'état de cet objet: **remember**.

Les deux variables: expanded et text sont des états. Le changement d'expanded va faire apparaître à l'écran le DropDownMenu (Spinner), il est affecté par l'utilisation de la fonction lambda affectée à clickable {}. En changeant la valeur de text à une nouvelle valeur, automatique le contenu de Text() est modifié, car sa valeur est "$text" comme texte.

## Réaction au clic

Deux façons de gérer les clics en compose:

* Un composant ayant une fonctionnalité de click tel qu'un bouton aura un paramètre onClick qui devra être défini.
* Un composant qui n'a généralement pas la fonctionné pourrait être modifié afin de le permettre.

```
@Composable
@Preview
fun ButtonDemo() {
    Box {
        Button(onClick={
            println("clicked")
        }){
            Text("Cliquez-moi")
        }
    }
}
```

[Exemple](../../Exemples/Android/Cours%204/Compose2)

Par contre dans l'exemple donné précédent, Text() n'a pas de possibilité cliquable, on ajoute à l'option modifier la possibilité de le rendre clickable.

## Recomposition

Comme dit précédemment, les objets se recomposent automatiquement lors de changement de valeur des objets states.

L'exemple 3 montre également comment partager entre deux éléments distincts la même information.

[Exemple](../../Exemples/Android/Cours%204/Compose3)