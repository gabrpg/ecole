## Fonctions

Les fonctions acceptent zéro, un ou plusieurs arguments et ils peuvent retourner zéro, un ou plusieurs éléments.

Voici la syntaxe pour la fonction addition:

```go
func add(x int, y int) int {
    return x + y
}
```

Ils peuvent également retourner plusieurs arguments, remarquez la syntaxe:

```go
func swap(x, y string) (string, string) {
	return y, x
}

func main() {
	a, b := swap("hello", "world")
	fmt.Println(a, b)
}
```

### Nombre d'arguments variable

Les fonctions variables sont un type de fonctions spéciales ayant un type particulier comme dernier élément: ce dernier peut être de taille variable (remarquez les ...) dans la méthode suivante:

```go
func add(args ...int) int {
  total := 0
  for _, v := range args {
     total += v
  }
  return total
}
func main() {
  fmt.Println(add(1,2,3))
}
```

Les ... indiquent que c'est un nombre variable d'arguments, on peut y passer un slice et il va l'accepter et retourner la donnée (total).

### Fonctions en ligne

Il est possible de créer des fonctions inline, i-e définis à même une fonction:

```go
func main(){
  add := func(x, y int) int {
    return x + y
  }

  fmt.Println(add(1,2))
}
```

ou

```go
func main() {
  x := 0
  increment := func() int {
    x++
    return x
  }
  fmt.Println(increment())
  fmt.Println(increment())
}
```

## Retour nommé

On peut également spécifier plusieurs variables de retour en go. Par contre, nous devons être précis quant au nom qu'on attribue à ses éléments de retour, il faut assurément être précis pour ses retours. Finalement, il faut utiliser un return sans argument pour retourner toutes les valeurs assignées de ses éléments.

```go
package main

import "fmt"

func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}

func main() {
	fmt.Println(split(17))
}
```

## defer

Une déclaration defer s'effectue seulement à la fin d'exécution de la fonction en cours:

```go
func main() {
  defer fmt.Println("world")
  fmt.Println("hello")
}
```

L'évaluation se fait immédiatement, mais s'exécute ultérieurement. L'exemple précédent va afficher Hello World

Les defer se font empilés de façon last in, first out. (une pile quoi!).

```go
func main() {
	fmt.Println("counting")
	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}
	fmt.Println("done")
}
```

Ceci affiche 9 8 7 6 ... 0 (en sautant une ligne à chaque fois).

### Fonction récursive

Une fonction peut s'autoappeler et ainsi créer des appels sur la pile d'exécution:

```go
func factorial(x uint) uint {
  if x == 0 {
    return 1
  }
  return x * factorial(x-1)
}
```

Go ne fait pas d'optimisation de récursion finale (*tail-recursive*). Mais utiliser cette pratique optimise un peu votre code. Voici la version récursion finale de la fonction factorial:

```go
func factTR(n uint, a uint) uint {
    if n == 1 {
        return a;
    }
    return factTR(n-1, n*a);
}

func factorial(x uint) uint {
    return factTR(x, 1)
}
```

La version "normale" prend 4.375 ns pour s'exécuter, tandis que la version avec récursion finale prend 2.646 ns pour s'exécuter, environ 60% plus rapide.