package fib

import "testing"

var resultat int

func benchmarkFib(i int, b *testing.B) {
	var r int

	for n := 0; n < b.N; n++ {
		r = Fib(i)
	}
	resultat = r
}
func BenchmarkFib1(b *testing.B)  { benchmarkFib(1, b) }
func BenchmarkFib2(b *testing.B)  { benchmarkFib(2, b) }
func BenchmarkFib3(b *testing.B)  { benchmarkFib(3, b) }
func BenchmarkFib10(b *testing.B) { benchmarkFib(10, b) }
func BenchmarkFib20(b *testing.B) { benchmarkFib(20, b) }
func BenchmarkFib40(b *testing.B) { benchmarkFib(40, b) }

func BenchmarkFib50(b *testing.B) { benchmarkFib(50, b) }
