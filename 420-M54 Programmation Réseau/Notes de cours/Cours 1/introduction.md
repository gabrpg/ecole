# Langage Go (introduction)

## Historique

Ce langage est typé et compilable développé par Google en 2007. La version 1.0 est sortie en 2012, c'est un langage informatique relativement nouveau. Il est disponible sur de multiples plateformes de système d'exploitation: Windows, GNU/Linux, macOS, BSD, ...

## Particularités

Il ressemble au C/C++, mais il a une meilleure protection de la mémoire graçe à son *garbage collector*. Il a également une meilleure gestion de la concurrence (de rouler plusieurs processus par la même application). Le dernier point étant particulièrement utile dans ce cours-ci étant donné que nous allons nous tourner vers de la programmation réseautique.

La majorité des exemples des notes peuvent facilement être copiés et collés sur le site: [https://play.golang.org/](https://play.golang.org/).

D'ailleurs vous avez la chance en chargeant cette page de voir le hello world:

```go
package main
import (
	"fmt"
)
func main() {
	fmt.Println("Hello, playground")
}
```

# Installation

## Solution native

Téléchargez go à partir du site officiel: [https://go.dev/](https://go.dev/).


# Paquetage
Tous vos programmes sont réalisés en paquetages (*packages*) :

```go
package main

import (
	"fmt"
	"math/rand"
)
func main() {
	fmt.Println("My favorite number is", rand.Intn(10))
}
```

Ici, il y a deux paquetages qui sont importés (qui proviennent de la librairie de go), fmt pour le formatage et le paquetage Rand contenu dans le paquetage Math, comme nous pouvons le voir dans le lien suivant:

https://golang.org/pkg/math/

Permettant ainsi ici de segmenter nos paquetages aisément en fonction de nos besoins (nos applications go pourraient être subdivisé en plusieurs paquets pour alléger le code et pour simplifier sa conception. Nous y reviendrons ultérieurement.

Il est à savoir que les imports non utilisés feront que votre code ne compilera pas, ceci est voulu dans le sens que vous ne voulez pas garder des importations augmentant la taille de vos exécutables.

```go
package hello

import "rsc.io/quote"

func main() {
	println(quote.Go())
}
```


## Noms exportés

Lorsque vous voulez que vos méthodes soient accessibles à l'extérieur de vos paquetages, vous devez les nommer avec une lettre majuscule.

Par exemple:

```go
package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(math.pi)
}
```

Dans cet exemple, math.pi est introuvable, car dans le paquet https://pkg.go.dev/math, pi n'existe pas, c'est Pi la constante.

# Langage Go

Il n'y a pas de point-virgule à la fin de chaque ligne.

Si vous voulez tester, utilisez le paquet fmt pour afficher une donnée dans la console. Voici le fameux "*hello world*".

```go
package main

import "fmt"

func main() {
    fmt.Println("Hello World!")
}
```

https://go.dev/play/

Pour exécuter, rouler la ligne suivante en ligne de commande, "main.go" est le nom de votre fichier à rouler, go va exécuter la fonction "main".

```bash
go run main.go
```

Pour lire dans la console (utile pour l'atelier 1), il faut aussi utiliser le paquet fmt:

```go
var s string
fmt.Scanln(&s)

var i int
fmt.Scanf("%d", &i)
```

## Types

Go à accès aux mêmes types de bases que dans la majorité des autres langages de programmation:

- int8, int16, int32, int64, int (u en devant pour les non-signés)
- rune (alias à int32) : ceci représente une valeur unicode
- float32, float64 (nombre à virgule)
- complex64, complex128
- bool
- string

Il n'y a pas de classes, mais le langage a un type nommé struct qui vous permettra d'obtenir à peu près les mêmes fonctionnalités. Nous y reviendrons dans un chapitre ultérieur.

## Déclaration

Elle peut être statique :

```go
var x float64
x = 20.0
```

Ou dynamique lorsque le compilateur peut déterminer le type par l'entremise de la valeur qui lui est assignée, dans ce cas si, il n'y a pas de déclaration avec var de la variable. En anglais on appelle cette déclaration une "*short variable declaration*".

```go
a := 4
s := "J'aime les patates"
```

Personnellement je préfère la version longue à la version courte. Parfois vous n'aurez pas le choix d'utiliser un style ou l'autre, par exemple vous devez utiliser la version longue pour les variables globales.

Pour rendre une variable immuable, suffit d'utiliser l'identifiant const avant la déclaration de la variable:

```go
const x string = "Bonjour monde!"
x = "Réutilisation de variable"
```

Il est possible également d'effectuer de multiple assignation automatique:

```go
var i, j int = 1, 2
x, y := 3,4
```
