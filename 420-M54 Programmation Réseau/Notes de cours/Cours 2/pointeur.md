## Pointers

Go a des pointeurs. Ils sont légèrement parsemés dans le langage. Un pointeur garde en mémoire l'adresse de la valeur.

Le type *T est un pointeur vers la valeur de T. Sa valeur zero est nil.

```go
var p *int
```

L'opérateur & génère un pointeur de son opérateur.

```go
i := 42
p = &i
```

The * operator denotes the pointer's underlying value.

```go
fmt.Println(*p) // lit i à travers le pointeur p
*p = 21         // assigne i à travers le pointeur p
```

Ceci est connu sous le terme "déréférencement".

Contrairement à C, Go ne fait pas d'arythmétique avec les pointeurs.

# Les opérateurs *, & et new

## \*

En Go, un pointeur est représenté en utilisant l'astérisque * suivi par le type de l'élément stocké.

\* est aussi utilisé pour déréférencer une variable pointeur. Le déréférencement permet d'accéder à la valeur pointé par le pointeur. Lorsque nous écrivons *abc = 1, ça indique stocker la valeur 1 dans l'adresse mémoire pointé par abc. Si l'on essaie de faire abc=1, nous allons obtenir une erreur de compilation car abc n'est pas un int, mais un *int pour lequel on peut assigner qu'une valeur *int.
  
## & 
Finalement, nous utilisons l'opérateur & afin d'obtenir l'adresse d'une variable. &x retourne un *int (pointeur à un int) car x est un int. C'est ce qui nous permettrait de modifier la valeur de x si ce dernier est passé en paramètre.

## new
Une autre façon d'obtenir un pointeur est d'utiliser la fonction new. new prend comme argument un Type, alloue de la mémoire pour affecter une valeur au type et retourne un pointeur à cet élément.

Dans quelques langages de programmation, il y a une grande différence entre new et & et nous devons éventuellement libérer la mémoire affectée par new. Go n'est pas comme ça, il y a un garbage collector qui nettoie les objets lorsque ceux-ci ne sont plus référés.

Les pointeurs sont peu utilisés avec les types par défaut de Go, mais nous allons voir qu'ils peuvent être utiles avec les structs.

[Exemples](../../Exemple/Cours%202/Pointeurs/)
