# Classes

On utilise le mot clé class:

```
class Animal {

  var couleur = ""
  var food = ""
  var habitat = ""

  fun crie() {
  }
  
  fun eat() {
  }
}
```

**Fait**

* Pas besoin de coder un constructeur, le compilateur en a ajouté un lui-même, nommé Animal()

# Constructeur

Chaque classe a un constructeur primaire et un ou plusieurs constructeurs secondaires.

Le constructeur se définit en paranthèse après le nom de la classe. Il va initier les variables situées après et assigner les getter et setter automatiquement.

[Exemple 1](../../Exemples/Kotlin/Cours%202/2.kt)

### Constructeur second
Ceci permet de définir un constructeur secondaire qui permet une nouvelle façon de définir un Constructeur.

```kotlin
class Person(val pets: MutableList<Pet> = mutableListOf())

class Pet {
    constructor(owner: Person) {
        owner.pets.add(this) // ajout cet animal à la liste des animaux du propriétaire
    }
}
```

## get / set

Le langage va créer les getter et setter automatiquement, mais vous pouvez toujours redéfinir leur comportement

```
  var nbrPattes = 0
    get() = field
  
    set(value) {
        field = value 
    }
```

[Exemple 2](../../Exemples/Kotlin/Cours%202/1.kt)

## Visibility

Les classes, les interfaces, les constructeurs et les fonctions tout comme les propriétés et leur setter peuvent avoir des modificateurs de visibilité. Les getters ont toujours les mêmes visibilités que la propriété.

Quatre possiblité:

* private signifie que les membres ne sont visibles que pour cette classe
* protected signifie que les membres ont la même visibilité que ceux privés mais aussi pour les sous-classes.
* internal signifie que chaque client dans le module qui voit la déclaration de la classe peut voir les membres internes.
* public signifie que tous les clients qui voient la déclaration de classe voit les membres publics.

Par défaut, les propriétés vus sont publiques.

## Instanciation

Il n'y a pas de new en Kotlin.

```kotlin
val animal = Animal()
```

## init

Le code à l'intérieur du bloc init est le premier à être exécuté quand la classe est instancié. Le code init est exécuté à chaque fois que la classe est instanciée.

The code inside the init block is the first to be executed when the class is instantiated. The init block is run every time the class is instantiated, with any kind of constructor as we shall see next.

```
class Person(val fullName: String) {
    var isLongName = false
    var isMature = true
    
    var age = 18
        get() {
            return field
        }

    // constructeur2
    constructor(fullName: String, age: Int) : this(fullName) {
        this.age = age
        this.isMature = this.age >= 18
    }
   
    init {
        isLongName = fullName.length > 15
    }
}

fun main() {
  // instance
  val john = Person("John", 12)
  
  print(john.age)
  print(john.isMature)
}

```

**Fait**

Ordre de création
* Premier constructeur
* init
* Second constructeur

## Héritage   

Pour hériter d'une classe, il faut assigner à cette classe le descriptif open. De plus, il faut assigner à chaque propriété et méthode le descriptif open afin de pouvoir la redéfinir. 

[Exemple 3](../../Exemples/Kotlin/Cours%202/3.kt)

**Fait**
* Vous pouvez déclarer une propriété comme étant final afin d'éviter qu'une propriété soit override.
* Les appels aux méthodes vont du plus précis au moins précis. Donc si vous appelez une méthode qui n'est pas définit dans l'enfant, mais qui est définit dans le parent, c'est celle du parent qui se fera appelée.

Le compilateur est assez brillant pour se rendre compte que si vous créez une liste contenant deux sous classes d'une classe, il va créer un arrayList de la classe.

[Exemple 4](../../Exemples/Kotlin/Cours%202/4.kt)

**FAIT**

* Tester si une variable est d'un type avec la méthode **IS**, si une classe est dérivée d'une autre, par exemple Hippo IS animal.

# Abstraction

Améliorons le code, il est possible dans la vraie vie de voir un hippo ou un chien, mais il est impossible de voir un "Animal" se promener, enfin vous comprenez.

On pourrait donc modifier notre code pour éviter qu'un utilisateur décide d'instancier un Animal en utilisant le mot-clé Abstract.

[Exemple 5](../../Exemples/Kotlin/Cours%202/5.kt)

**Fait**
* La méthode *dort()* doit restée présente et ne pas être abstraite car nous ne l'avons pas redéfinie partout.
* Le compilateur n'accepte pas que vous commenciez à écrire du code pour une fonction abstraite.
* Si une fonction ou une propriété est abstraite, toute la classe doit le devenir.
* Pour ce qui est des classes concrètes, vous devrez implémenter toutes les méthodes et propriétés abstraites que les classes abstraites desquelles vous dérivez. Si C dérive de B et que B dérive de A. B pourrait définir les méthodes abstraites de A, même si elle même est abstraite.

# Interface

Si vous désirez implémenter un même comportement entre deux classes qui n'ont aucun lien entre elle dans la hiérarchie, vous devez utiliser une interface. Par exemple dans notre cas, déplace() qui pourrait être rattachée à une certaine vitesse de déplacement. Cependant, puisque les animaux peuvent avoir une vitesse de déplacement, tout comme les voitures... nous allons donc offrir une interface 

[Exemple 6](../../Exemples/Kotlin/Cours%202/6.kt)

## Comment savoir si l'on doit faire une classe, une classe enfant, une classe abstraite ou une interface?

* Faire une classe sans classe parent lorsqu'elle n'est pas du type parent. Par exemple, une auto n'est pas un animal.
* Faire une sous-classe lorsqu'elle doit héritée d'une classe parent quand vous devez avoir une classe plus spécifique et que vous devez modifier ou ajouter des nouveaux comportements
* Faire une classe abstraite quand vous souhaitez définir un modèle pour un groupe de classe, elle peut être utilisée pour être certain lorsque vous voulez qu'une classe ne peut être instanciéee.
* Faire une interface quand vous voulez que plusieurs classes puissent partager un comportement peu importe où elles sont situées dans la hiérarchie de classe.

Vous pouvez faire des petits tours de passes passes avec les interfaces:

[Exemple 7](../../Exemples/Kotlin/Cours%202/7.kt)

# Any

Chaque classe hérite de Any, même si vous ne le spécifier par

```
class MyClass {

}
```

est en réalité

```
class MyClass: Any {


}
```

## Pourquoi ?

* Ça permet de s'assurer que toutes les classes ont un minimum de comportement commun, comme par exemple la fonction equals() qui permet de vérifier si deux classes sont identiques.

* Ça permet d'exécuter du polymorphisme avec un ensemble d'objet même si rien ne semble les relier:

```
val monTableau = arrayOf(Baton(), Orc(), Piano())
```

Le compileur va déterminer que la classe Any est celle qui unit les 3, va donc créer un tableau de type Any: Array<Any>.

### equals(any: Any): Boolean

C'est une méthode qui permet de recevoir un objet de type Any, que vous pourriez caster en un autre objet en utilisant la méthode IS (testé) ou AS (forcé).

Par défaut, il compare si les deux objets:
* l'objet sur lequel on appelle la méthode 
* l'objet passé en paramètre
* ont la même référence, si c'est le cas: return true, sinon return false.

C'est cette méthode qui est appelée lorsqu'on utilise le ==.

### hashCode(): Int

Retourne un hash de la classe. Peut être utilisé par une structure pour stocker et retrouver les valeurs plus efficacement.

### toString(): String

Retourne une String qui représente l'objet.

# Data class

Bien que l'on pourrait override la méthode equals pour vérifier des conditions précises (si deux propriétés sont équivalentes, la classe est identique). Les développeurs kotlin ont créé un nouvel objet qui va simplifier la comparaison. Si une classe sert uniquement à stocker du contenu, vous pourriez utiliser le data class.

Une data class n'utilise que les valeurs des propriétés pour vérifier que deux objets sont équivalents.

De plus, deux objets identiques vont retourner deux hashCode() identiques.

Finalement, si vous voulez vous assurez que la référence est la même entre deux objets, vous pouvez toujours utiliser le === lors de la comparaison (identité).

[Exemple 8](../../Exemples/Kotlin/Cours%202/8.kt)
