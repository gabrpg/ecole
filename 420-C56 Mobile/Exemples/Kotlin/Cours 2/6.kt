/**
 * You can edit, run, and share this code.
 * play.kotlinlang.org
 */

 interface Roamable {
    fun deplace()
}

abstract class Animal {

  abstract var couleur: String
  abstract var food: String
  abstract var habitat: String

  abstract fun crie()  
  abstract fun mange()
  
  open fun dort() {
    println("Il dort")
  }
}

class Hippo : Animal() {
  override var couleur = "gris"
  override var food = "herbe"
  override var habitat = "eau des lacs africains"

  override fun crie() {
    println("L'hippopotame émet des mugissements")
  }

  override fun mange() {
    println("L'hippopotame mange de $food")
  }
  
}

class Chien : Animal(), Roamable {
  override var couleur = "blanc"
  override var food = "moulée"
  override var habitat = "maison"

  override fun crie() {
    println("Le chien jappe")
  }

  override fun mange() {
    println("Le chien mange la $food")
  }

  override fun deplace() {
      println("Le chien peut se déplacer")
  }
}


fun main(){
    var hippo = Hippo()
    
    hippo.crie()
    hippo.mange()
    hippo.dort()
    
    var animaux = listOf(Hippo(), Chien())
    println(animaux.javaClass.simpleName)
    
    for (animal in animaux) {
    	animal.crie()
        // animal.deplace()
        if (animal is Chien) 
        	animal.deplace()
    }
    
    
    
    //var animal = Animal()
    
}