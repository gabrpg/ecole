package main

import (
	"fmt"
	"math"
)

type Cercle struct {
	x, y, r float64
}

type Rectangle struct {
	x1, y1, x2, y2 float64
}

type Forme interface {
	aire() float64
}

func (c *Cercle) aire() float64 {
	return math.Pi * c.r * c.r
}

func (r *Rectangle) aire() float64 {
	return (r.x2 - r.x1) * (r.y2 - r.y1)
}

func aireTotale(formes ...Forme) float64 {
	var total float64
	for _, f := range formes {
		total += f.aire()
	}
	return total
}

func main() {
	c := &Cercle{0, 0, 5}
	r := &Rectangle{10, 10, 20, 20}

	fmt.Println(aireTotale(c, r))
}
