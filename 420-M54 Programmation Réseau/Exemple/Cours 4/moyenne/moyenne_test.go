package main

import (
	"testing"
)

func TestMoyenne(t *testing.T) {
	v := Moyenne([]float64{1, 2})
	if v != 1.5 {
		t.Error("Attendu 1.5, Obtenu ", v)
	}
}

type paires struct {
	valeurs []float64
	moyenne float64
}

var tests = []paires{
	{[]float64{1, 2}, 1.5},
	{[]float64{1, 1, 1, 1, 1, 1}, 1},
	{[]float64{-1, 1}, 0},
	{[]float64{0}, 0},
}

func TestMoyenne2(t *testing.T) {
	for _, paire := range tests {
		v := Moyenne(paire.valeurs)
		if v != paire.moyenne {
			t.Error("Pour", paire.moyenne, "attendu ", paire.moyenne, "reçu ", v)
		}
	}
}
